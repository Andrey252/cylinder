package ru.andrey.dev;

import java.util.Scanner;

/**
 * Класс нахождения площади поверхности и объёма цилиндра.
 *
 * @autor А. Рыжкин
 * @since 01.09.2016
 */

public class Cylinder {
    /**
     * radius - радиус цилиндра.
     * height - высота цилиндра.
     */
    private int radius;
    private int height;

    /**
     * Начальные значения радиуса и высоты цилиндра.
     * radius = 0;
     * height = 0;
     */
    public Cylinder() {
        this(0, 0);
    }

    public Cylinder(int radius, int height) {
        this.radius = radius;
        this.height = height;
    }

    /**
     * Метод нахождения площади боковой поверхности цилиндра.
     *
     * @return возращает высчитанную площадь боковой поверхности цилиндра.
     */
    public int sideSurfaceArea() {
        return (int) (2 * Math.PI * radius * height);
    }

    /**
     * Метод нахождения площади основании цилиндра.
     *
     * @return возращает высчитанную площадь основании цилиндра.
     */
    public int baseArea() {
        return (int) (2 * Math.PI * radius * radius * height);
    }

    /**
     * Метод нахождения объёма цилиндра.
     *
     * @return возращает высчитанный объём цилиндра.
     */
    public int amount() {
        return (int) (Math.PI * radius * radius * height);
    }

    /**
     * Метод ввода данных цилиндра.
     */
    public void input() {
        Scanner reader = new Scanner(System.in);
        do {
            radius = reader.nextInt();
        }
        while (checkInput(radius));

        do {
            System.out.print("Enter the height of the cylinder (mm): ");
            height = reader.nextInt();
        }
        while (checkInput(height));
 //      reader.close();
    }

    /**
     * Метод проверки на ввод исходных данных.
     *
     * @param check значение для проверки.
     * @return возращает булевое значение.
     */
    public boolean checkInput(int check) {
        if (check < 0) {
            System.out.println("Incorrect value!");
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "{" +
                "radius=" + radius + " mm" +
                ", height=" + height + " mm" + '}';
    }
}