package ru.andrey.dev;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Cylinder trumpet = new Cylinder();
        Cylinder cylinder = new Cylinder();

        System.out.print("Enter the inner radius trumpet (mm): ");
        trumpet.input();
        System.out.print("Enter the outer radius cylinder (mm): ");
        cylinder.input();

        System.out.println("Trumpet " + trumpet.toString());
        System.out.println("Cylinder " + cylinder.toString());

        System.out.println("Area = " + (trumpet.sideSurfaceArea() + cylinder.sideSurfaceArea() + cylinder.baseArea()) + " mm^2");
        System.out.println("Amount = " + (cylinder.amount() - trumpet.amount()) + " mm^3");
    }
}